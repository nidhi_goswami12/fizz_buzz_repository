﻿namespace FizzBuzzWeb.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class FizzBuzzWebConstants
    {
        public const int PageSize = 20;
    }
}