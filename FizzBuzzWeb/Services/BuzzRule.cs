﻿namespace FizzBuzzWeb.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class BuzzRule : IRule, IDateTimeService
    {
        private readonly IDateTimeService dateTimeService;

        public BuzzRule(IDateTimeService dateTimeService)
        {
            this.dateTimeService = dateTimeService;
        }

        public bool IsMatch(int number)
        {
            return number % 5 == 0;
        }

        public string Execute()
        {
            var dayOfWeek = this.dateTimeService.GetDateTimeNow().DayOfWeek;
            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                return "wuzz";
            }
            else
            {
                return "buzz";
            }
        }

        public DateTime GetDateTimeNow()
        {
            throw new NotImplementedException();
        }
    }
}
