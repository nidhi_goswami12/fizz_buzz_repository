﻿namespace FizzBuzzWeb.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IRule
    {
        bool IsMatch(int input);

        string Execute();
    }
}
