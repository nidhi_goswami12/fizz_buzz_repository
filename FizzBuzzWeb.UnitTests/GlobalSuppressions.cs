﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzBuzzRuleTest.FizzBuzzRuleTest")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.UnitTests.Services.Controllers.FizzBuzzWebControllerTest.WhetherModelsUpdatedWithExpectedFizzBuzzNumbers")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.UnitTests.Services.DateTimeServiceTest.WhetherGetDateTimeReturnsCurrentInput")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.Controllers.FizzBuzzWebControllerTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.BuzzRuleTest")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1208:System using directives should be placed before other using directives", Justification = "not required", Scope = "namespace", Target = "~N:FizzBuzzWeb.UnitTests.Services")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.DateTimeServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1129:Do not use default value type constructor", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.UnitTests.Services.DateTimeServiceTest.WhetherGetDateTimeReturnsCurrentInput")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzBuzzRuleTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzBuzzServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.UnitTests.Services.FizzRuleTest.ExecuteShouldReturnValueBasedOnWednesday(System.String,System.String)")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzRuleTest")]

//[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "member", Target = "~M:FizzBuzzWeb.UnitTests.Services.FizzRuleTest.IsMatchShouldReturnValueBasedOnInput(System.Int32,System.Boolean)")]
//[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not required", Scope = "type", Target = "~T:FizzBuzzWeb.UnitTests.Services.FizzRuleTest")]
